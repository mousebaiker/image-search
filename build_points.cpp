/****
* build_points.cpp
*
* Computes keypoints and descriptors for the whole database. Chucks out db
* in roughly the same blocks with 1000 images in each.
*
* Aleksei Kalinov, 2016
**/

#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;

int main(void)
{

	// Creating chunks
	for (int j = 1; j < 10; j++)
	{
		FileStorage output_file_small("points_sift_" + std::to_string(j) + ".yml", FileStorage::WRITE);
		Mat input_image;
		vector<KeyPoint> keypoints;
		Mat descriptors;
		SiftFeatureDetector detector(0, 3, 0.18, 10);
		SiftDescriptorExtractor extractor;
		string number;

		// Compute keypoints and descriptors for 1000 pictures from that chunk.
		for (int i = j*1000; i < (j + 1)*1000; i++)
		{
			number = std::to_string(i);
			input_image = imread("../kinopoisk/" + number + ".jpg");

			if (input_image.data)
			{
				detector.detect(input_image, keypoints);
				write(output_file_small , "k" + number, keypoints);


				extractor.compute(input_image, keypoints, descriptors);
				write(output_file_small, "d" + number, descriptors);
        std::cout << keypoints.size() << std::endl;
			}
			if (i%50 == 0)
			{
				std::cout << std::endl <<  "Executed" << number << std::endl;
			}
		}
		output_file_small.release();

		std::cout << "Done " << j * 1000 << " - " << (j + 1) * 1000 << "Starting next" << std::endl;
	}
}
