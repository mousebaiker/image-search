/****
* clustering.cpp
*
* Clusters descriptors using kmeans and stores the result in a file.
*
* Aleksei Kalinov, 2016
**/
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <iostream>

using std::to_string;
using std::vector;
using namespace cv;

int main(void)
{
  // Handle the input
  char* input_name = (char*) "points_sift_";
  Mat descriptors;
  Mat image_descriptors;
  for(int j = 1; j < 10; j++)
  {
    FileStorage database(input_name + to_string(j) + ".yml", FileStorage::READ);
    FileNode node_d;

    for (int i = j*1000; i < (j+1)*1000; i++)
    {
      std::cout << i << std::endl;

      node_d = database["d" + to_string(i)];
      read(node_d, descriptors);
      image_descriptors.push_back(descriptors);

      std::cout << i << " " << descriptors.rows << std::endl;
    }

    std::cout << image_descriptors.size() << std::endl;
    database.release();
  }
  Mat output_labels;
  Mat centers;

  kmeans(image_descriptors, 1000, output_labels, TermCriteria(CV_TERMCRIT_ITER, 10, 0.01), 10, KMEANS_RANDOM_CENTERS, centers);

  // Storing the results
  FileStorage clustering_info("clusters.yml", FileStorage::WRITE);
  write(clustering_info, "labels", output_labels);
  write(clustering_info, "centers", centers);
  clustering_info.release();
}
