/****
* gradients.cpp
*
* Produces gradient matrix from the input file.
*
* Aleksei Kalinov, 2016
**/
#include <cv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

char* DEFAULT_PICTURE = (char *) "../kinopoisk/298.jpg";
const int scale = 1;
const int delta = 0;
const int kernel_size = 3;
const int gauss_kernel_size = 3;
using namespace cv;


/**
* Computes gradient matrix values of the input using Sobel operator. Returns 32F matrix, so don't forget to scale back, if necessary.
**/
void gradient_mat_values(Mat input, Mat& output, bool gray = true, bool smoothing = false)
{
	Mat gradient_x, gradient_y;

	// Make smoothing to reduce noise
	if (smoothing)
	{
		GaussianBlur(input, input, Size(gauss_kernel_size, gauss_kernel_size), 0, 0, BORDER_DEFAULT);
	}

	// Convert to gray, if needed
	if (gray)
	{
		cvtColor(input, input, CV_BGR2GRAY);
	}

	// Horizontal and vertical gradients
	Sobel(input, gradient_x, CV_32F, 1, 0, kernel_size, scale, delta, BORDER_DEFAULT);
	Sobel(input, gradient_y, CV_32F, 0, 1, kernel_size, scale, delta, BORDER_DEFAULT);

	// Real values of the gradient vectors
	gradient_x = gradient_x.mul(gradient_x);
	gradient_y = gradient_y.mul(gradient_y);
	output = gradient_x + gradient_y;
	pow(output, 0.5, output);
}

/***
*Demonstrates usage of gradient_mat_values function
**/
int main(int argc, char* argv[])
{
	char* input_name = DEFAULT_PICTURE;
	if (argc != 1)
	{
		input_name = argv[1];
	}

 	Mat input_image = imread(input_name);
	if (!input_image.data)
	{
		return 1;
	}

	Mat gradient;
	gradient_mat_values(input_image, gradient, false, true);
	convertScaleAbs(gradient, gradient);
	namedWindow("Input", CV_WINDOW_NORMAL);
	imshow("Input", input_image);
	namedWindow("Test", CV_WINDOW_NORMAL);
	imshow("Test", gradient);
	imwrite("gradient.jpg", gradient);
	waitKey(0);
}
