#include <opencv2/opencv.hpp>
#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;
using std::sort;
using std::vector;
using std::string;
using std::to_string;
using std::pair;

void build_points(Mat& image, vector<KeyPoint>& keypoints, Mat& descriptors)
{
  SiftFeatureDetector detector(0, 3, 0.18, 10);
  SiftDescriptorExtractor extractor;

  detector.detect(image, keypoints);
  extractor.compute(image, keypoints, descriptors);
}

float distance(float* f, float* a, int size)
{
  float result = 0;
  for (int i = 0; i < size; i++)
  {
    result += (f[i] - a[i])*(f[i] - a[i]);
  }
  return result;
}

float distance_v(vector<float> f, vector<float> a)
{
  float result = 0;
  for (int i = 0; i < f.size(); i++)
  {
    result += (f[i] - a[i])*(f[i] - a[i]);
  }
  return result;
}

bool distance_comp(pair<int, float> a, pair<int, float> b)
{
  return a.second < b.second;
}

void compute_clusters(Mat& descr, Mat& centers, vector<int>& clusters)
{
  for (int i = 0; i < descr.rows; i++)
  {
    float min_distance = distance(descr.ptr<float>(i), centers.ptr<float>(0), 128);
    int min_index = 0;
    float current_distance;
    for (int j = 0; j < centers.rows; j++)
    {
      current_distance = distance(descr.ptr<float>(i), centers.ptr<float>(j), 128);
      if (current_distance < min_distance)
      {
        min_distance = current_distance;
        min_index = j;
      }
    }
    clusters.push_back(min_index);
  }
}

void build_word(vector<int>& clusters, vector<float>& word, vector<float>& idf_values)
{

  //std::cout << "Sizes: " << clusters.size() << " " << word.size() << " " << idf_values.size() << std::endl;
  word = vector<float>(1000, 0);
  for (int j = 0 ; j < clusters.size(); j++)
  {
    word[clusters[j]] += 1.0;
  }

  for (int j = 0; j < word.size(); j++)
  {
    word[j] *= idf_values[j]/(float) clusters.size();
  }
}


void rank_distance(vector<float>& word, vector< vector<float> >& words, vector< pair<int, float> >& result)
{
  float current_distance;
  for (int i = 0; i < words.size(); i++)
  {
    current_distance = distance_v(word, words[i]);
    result.push_back(pair<int, float>(1000 + i, current_distance));
  }
  sort(result.begin(), result.end(), distance_comp);
  return;
}

int rank_geometrically(vector<KeyPoint> keypoints, Mat descriptors,
  vector< pair<int, float> > sorted_images)
{
  int max_result = 0;
  int result_image = 0;

  for (int j = 0; j < 20; ++j)
  {
    Mat image = imread("../kinopoisk/" + to_string(sorted_images[j].first) + ".jpg");
    if (!image.data) continue;
    vector<KeyPoint> candidate_keypoints;
    Mat candidate_descriptors;

    GaussianBlur(image, image, Size(5,5), 1.5);
    build_points(image, candidate_keypoints, candidate_descriptors);
    if (candidate_descriptors.type() == 0) continue;
    FlannBasedMatcher matcher;
    vector< vector<DMatch> > matches;
    matcher.knnMatch(descriptors, candidate_descriptors, matches, 2);

    vector<DMatch> good_matches;
    for (int i = 0; i < matches.size(); ++i)
    {
      if (matches[i][0].distance < 0.8*matches[i][1].distance)
      {
        good_matches.push_back(matches[i][0]);
      }
    }

    if (good_matches.size() >= 4)
    {
      vector<Point2f> first_object;
      vector<Point2f> second_object;
      for(int i = 0; i < good_matches.size(); ++i)
      {
        first_object.push_back(keypoints[good_matches[i].queryIdx].pt);
        second_object.push_back(candidate_keypoints[good_matches[i].trainIdx].pt);
      }

      Mat mask;
      Mat homography_matrix = findHomography(first_object, second_object, CV_RANSAC, 3, mask);

      int result = 0;
      for (int i = 0; i < mask.rows; ++i)
      {
        result += mask.ptr<uchar>(i)[0];
      }

      if (result > max_result)
      {
        max_result = result;
        result_image = sorted_images[j].first;
      }
    }
  }

  return result_image;
}

int search(string path, bool verbose)
{
  Mat image = imread(path);
  if(!image.data)
  {
    if (verbose)
    {
      std::cout << "Image is not valid" << std::endl;
    }
    return -1;
  }

  GaussianBlur(image, image, Size(5,5), 1.5);
  if (verbose)
  {
    std::cout << "Detecting points and describing them" << std::endl;
  }
  vector<KeyPoint> keypoints;
  Mat descriptors;
  build_points(image, keypoints, descriptors);

  FileStorage clustering_file("clust.yml", FileStorage::READ);
  FileNode centers_node = clustering_file["centers"];
  Mat centers;

  if (verbose)
  {
    std::cout << "Reading centers" << std::endl;
  }
  read(centers_node, centers);
  clustering_file.release();

  if (verbose)
  {
    std::cout << "Computing clusters" << std::endl;
  }
  vector<int> clusters;
  compute_clusters(descriptors, centers, clusters);

  FileStorage word_file("tfidf.yml", FileStorage::READ);
  FileNode idf_node = word_file["nyan_idf"];
  vector<float> idf_values;
  if (verbose)
  {
    std::cout << "Reading IDF values" << std::endl;
  }
  read(idf_node, idf_values);

  if (verbose)
  {
    std::cout << "Reading words" << std::endl;
  }

  vector< vector<float> > words;
  for (int j = 1; j < 10; j++)
  {
    if (verbose)
    {
      std::cout << "  Reading " << j*1000 << "-" << (j + 1)*1000 -1 << std::endl;
    }
    for (int i = j*1000; i < (j+1)*1000; i++)
    {
      vector<float> word;
      FileNode word_node = word_file["nyan" + to_string(i)];
      read(word_node, word);
      words.push_back(word);
    }
  }
  word_file.release();

  if (verbose)
  {
    std::cout << "Building a word" << std::endl;
  }
  vector<float> word;
  build_word(clusters, word, idf_values);

  if (verbose)
  {
    std::cout << "Finding closest" << std::endl;
  }
  vector< pair<int, float> > result;
  rank_distance(word, words, result);
  if (verbose)
  {
    std::cout << "\n\nPerhaps, your picture is one of the following: " << std::endl;
    for (int k = 0; k < 3; k++)
    {
      std::cout << "http://www.kinopoisk.ru/film/" << result[k].first << "/ \n";
    }

    std::cout << std::endl << "Wait to get better results" << std::endl << std::endl;
    std::cout << "Ranking geometrically" << std::endl;
  }

  return rank_geometrically(keypoints, descriptors, result);
}

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cout << "Usage: ./search name_of_the_image_to_search" << std::endl;
    return 1;
  }

  string image_path(argv[1]);
  int result = search(image_path, true);
  if (result)
  {
    std::cout <<  "Suggested film: http://www.kinopoisk.ru/film/" <<  to_string(result) << "/" << std::endl;
  }
  else
  {
    std::cout << "Could not find better picture" << std::endl;
  }
}
