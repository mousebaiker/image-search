/****
* detect_descript.cpp
*
* Implements wrappers around most popular detectors.
*
* Aleksei Kalinov, 2016
**/
#include <opencv2/opencv.hpp>
#include "opencv2/nonfree/nonfree.hpp"
#include <vector>
#include <string>
#include <iostream>

using std::string;
using std::vector;
using namespace cv;

char* DEFAULT_PICTURE = (char*) "../kinopoisk/325.jpg";


/**
* Detects keypoints using a series of surfDetectors with different Hessians,
* stepping 100 at a time and draws the keypoints on the image.
**/
void surfRangeDraw(Mat image, int start, int end, vector< vector<KeyPoint> >& keypoints, vector<Mat>& output)
{
    int steps = (end - start)/100 + 1;

    for (int i = 0; i < steps; i++)
    {
        SurfFeatureDetector detector(start + 100*i);
        detector.detect(image, keypoints[i]);
        drawKeypoints(image, keypoints[i], output[i], Scalar::all(-1), DrawMatchesFlags::DEFAULT);
    }
}

/**
* Detects keypoints using a Harris detector and draws the keypoints on the image.
**/
void Harris(Mat image, vector<KeyPoint>& keypoints, Mat& output)
{
    GoodFeaturesToTrackDetector detector(1000, 0.01, 1.0, 3, true);
    detector.detect(image, keypoints);
    drawKeypoints(image, keypoints, output, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
}

/**
* Detects keypoints using a Sift detector and draws the keypoints on the image.
**/
void Sift(Mat image, vector<KeyPoint>& keypoints, Mat& output)
{
    SiftFeatureDetector sift_detector;
    sift_detector.detect(image, keypoints);
    drawKeypoints(image, keypoints, output, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
}

/**
* Demonstrates usage of various detectors and computes keypoints on some examples.   
**/
int main(int argc, char* argv[])
{
    char* input_name = DEFAULT_PICTURE;

    if (argc != 1)
    {
        input_name = argv[1];
    }

    Mat input_image = imread(input_name);

    if (!input_image.data)
    {
        return 1;
    }


    /*
    vector< vector<KeyPoint> > keypoints(10);
    vector<Mat> img_keypoints(10);


    surfRangeDraw(input_image, 500, 1000, keypoints, img_keypoints);
    for(int i = 0; i < 6; i++)
    {
        namedWindow("Hessian = " + std::to_string(i*100), CV_WINDOW_NORMAL);
        imshow("Hessian = " + std::to_string(i*100), img_keypoints[i]);
        std::cout << keypoints[i].size() << std::endl;
        //imwrite("325_h_700_rich.jpg", img_keypoints[i]);
    }


    GoodFeaturesToTrackDetector detector(1000, 0.01, 1.0, 3, true);

    Harris(input_image, keypoints[0], img_keypoints[0]);
    imshow("Harris", img_keypoints[0]);
   // imwrite("outputs/325_good_1000.jpg", img_keypoints[0]);
    std::cout << keypoints[0].size() << std::endl;

    Sift(input_image, keypoints[2], img_keypoints[2]);
    namedWindow("SIFT", CV_WINDOW_NORMAL);
    imshow("SIFT", img_keypoints[2]);

   // imwrite("outputs/325_good_1000.jpg", img_keypoints[0]);
    std::cout << keypoints[2].size() << std::endl;

    waitKey(0);
    */

    vector <KeyPoint> keypoints;
    SiftFeatureDetector detector;
    detector.detect(input_image, keypoints);

    SiftDescriptorExtractor extractor;


    Mat descriptors;
    extractor.compute(input_image, keypoints, descriptors);

    std::cout << keypoints.size() << ' ' <<  descriptors.size() << std::endl;
	for (int i = 0; i < keypoints.size(); i++)
	{
		std::cout << keypoints[i] << ' ';
	}
    return 0;
}
