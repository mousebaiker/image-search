## Output files
The first number in each file indicates the number of the picture in database.

1. Files with suffix **\_h\_*number*.jpg** are files with keypoints calculated with SURF detector.
The number is the Hessian used.
2. **.txt** files contain the number of keypoints for the specified pictures when calculated using SURF.
Each row is for each Hessian (from 500 to 1000, step 100).
3. Files with suffix **\_good\_*number*.jpg** are files with keypoints calculated with GoodFeaturesToTrackDetector detector.
The number is the number of features used.
4. Files ending in **\_sift.jpg** are files with keypoints calculated with SIFT detector.
5. There is a special suffix **\_rich**, which indicates that keypoints are displayed in rich format.
6. Other files are quite self-descriptive by their file names.
