## Movie posters image search

This repo currently contains several files, which are intermediate steps towards building the image search application.

1. **basic_func.cpp** - Rotation and negative of the specified picture. See example *outputs/rotated.jpg* or *outputs/negative.jpg*
2. **gradients.cpp** - Calculation of the image gradient. The picture could be smoothed by GaussianBlur if required.  See example *outputs/gradient.jpg*
3. **detect_descript.cpp** - Wrappers for several detectors and the actual calculation of keypoints using various descriptors. See *ouptuts/README* for detailed explanation of examples.
4. **build_points.cpp** - Calculation of the image keypoints and its descriptors for the whole database. (DB and produced files are not included due to the size)
