#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

using std::string;
using std::to_string;
using std::vector;
using namespace cv;

int main(void)
{
  vector < vector<double> > tf_idf;
  string input_name = "words.yml";
  FileStorage words(input_name, FileStorage::READ);
  for (int j = 1; j < 10; ++j)
  {
    for (int i = j*(1000); i < (j+1)*(1000); ++i)
    {
      tf_idf.push_back(vector<double>(1000));
      FileNode word = words["word" + to_string(i)];
      read(word, tf_idf[i - 1000]);
    }
  }

  vector< vector<int> > lookup_index(1000);
  for (int i = 0; i < tf_idf.size(); ++i)
  {
    for (int j = 0; j < 1000; ++j)
    {
      if (tf_idf[i][j] != 0)
      {
        lookup_index[j].push_back(i + 1000);
      }
    }
  }

  FileStorage inverse("inverse_index.yml", FileStorage::WRITE);
  for (int i = 0; i < 1000; ++i)
  {
    write(inverse, "cluster" + to_string(i), lookup_index[i]);
  }
  inverse.release();
}
