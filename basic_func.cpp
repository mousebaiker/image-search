/****
* basic_func.cpp
*
* Produces negative of the picture, scales it down and rotates it.
*
* Aleksei Kalinov, 2016
**/
#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, char* argv[])
{
    Mat input;
    if (argc == 1)
    {
	  input = imread("first.jpg");
    }
    else
    {
      input = imread(argv[1]);
    }

    if (!input.data)
    {
       return 1;
    }
    imshow("Input picture", input);
    waitKey(0);

    // Inverting every single channel of every pixel
    for (int row = 0; row < input.rows; row++)
    {
        for (int column = 0; column < input.cols; column++)
        {
           for (int i = 0; i < 3; i++)
           {
               input.at<Vec3b>(row, column)[i] = 255 - input.at<Vec3b>(row, column)[i];
           }
        }
    }

    namedWindow("Negative picture", WINDOW_NORMAL);
    imshow("Negative picture", input);
    imwrite("negative.jpg", input);
    waitKey(0);

    Mat resized = Mat::zeros(input.rows, input.cols, input.type());
    double angle = 30.0;
    double scale = 2/3.0;
    Point center = Point(input.cols/2, input.rows/2);

    // Resizing and rotating
    Mat rotation = getRotationMatrix2D(center, angle, scale);
    warpAffine(input, resized, rotation, resized.size());
    imshow("Rotated and resized", resized);
    imwrite("rotated.jpg", resized);
    waitKey(0);
}
