/****
* matching.cpp
*
* Finds matches on two different pictures.
*
* Aleksei Kalinov, 2016
**/

#include <vector>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/nonfree/nonfree.hpp"

using std::vector;
using namespace cv;

char* DEFAULT_PICTURES[2] = {(char*) "../kinopoisk/298.jpg", (char*)"../kinopoisk/325.jpg"};

//TODO: Use keypoints and descriptors from db.
int main(int argc, char* argv[])
{
  char* names[2];
  for (int i = 0; i < 2; i++)
  {
    names[i] = DEFAULT_PICTURES[i];
  }
  if(argc == 3)
  {
    names[0] = argv[1];
    names[1] = argv[2];
  }

  Mat images[2];
  images[0] = imread(names[0]);
  images[1] = imread(names[1]);

  // Compute features and descriptors
  SiftFeatureDetector detector;
  SiftDescriptorExtractor extractor;
  vector<KeyPoint> keypoints[2];
  Mat descriptors[2];

  for (int i = 0; i < 2; i++)
  {
    detector.detect(images[i], keypoints[i]);
    extractor.compute(images[i], keypoints[i], descriptors[i]);
  }

  // Find matches
  FlannBasedMatcher matcher;
  vector<DMatch> matches;
  matcher.match(descriptors[0], descriptors[1], matches);

  int min_distance = 100;
  for (int i = 0; i < matches.size(); i++)
  {
    if (min_distance > matches[i].distance)
    {
      min_distance = matches[i].distance;
    }
  }

  // Pick suitable matches below threshold
  int threshold = 2*min_distance;
  vector<DMatch> suitable;
  for (int i = 0; i < matches.size(); i++)
  {
    if (matches[i].distance < threshold)
    {
      suitable.push_back(matches[i]);
    }
  }

  // Produce pictures with matches on them, ignoring single points
  Mat match_image;
  drawMatches(images[0], keypoints[0], images[1], keypoints[1], suitable, match_image, Scalar::all(-1), Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

  std::cout << suitable.size() << std::endl;
  namedWindow("Matches", CV_WINDOW_NORMAL);
  imshow("Matches", match_image);
  imwrite("matched.jpg", match_image);
  waitKey(0);
}
