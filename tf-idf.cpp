/****
* tf-idf.cpp
*
* Builds bag of words for images. Uses TF-IDF metrics.
*
* Aleksei Kalinov, 2016
**/
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <iostream>
#include <cmath>

using std::to_string;
using std::vector;
using namespace cv;

int tf(Mat labels, vector<int>& sizes, vector< vector<float> >& result)
{
  int empty_rows_number = 0;
  int j = 0;
  for (int i = 0; i < sizes.size(); i++)
  {
      // Build the word from the cluster info
      result.push_back(vector<float>(1000, 0));
      for (int n = j + sizes[i]; j < n; j++)
      {
        int* current_row = labels.ptr<int>(j);
        result[i][current_row[0]] += 1.0;
      }

      // Apply TF
      if (sizes[i] != 0)
      {
        for (int k = 0; k < 1000; k++)
        {
          result[i][k] /= (float) sizes[i];
        }
      }
      else
      {
        empty_rows_number++;
      }
  }

  return ((int) sizes.size()) - empty_rows_number;
}

void tf_idf(vector< vector<float> >& tf, int number_of_nonempty_rows, vector<float>& counters)
{

  for (int i = 0; i < 1000; i++)
  {
    // Count the number of documents where the word appears
    int counter = 0;
    for (int j = 0; j < tf.size(); j++)
    {
      if (tf[j][i] != 0)
      {
        counter += 1;
      }
    }

    // Apply IDF
    counters.push_back(0);
    if (counter != 0)
    {
      float idf_value = log2(number_of_nonempty_rows/(float) counter);
      counters[i] = idf_value;
      for (int j = 0; j < tf.size(); j++)
      {
        tf[j][i] *= idf_value;
      }
    }
  }
}
int main(void)
{

  // Retrieve the information about sizes
  char* input_name = (char*) "points_sift_";
  vector <int> sizes;
  Mat descriptors;
  for(int j = 1; j < 10; j++)
  {
    FileStorage database(input_name + to_string(j) + ".yml", FileStorage::READ);
    FileNode node_d;

    for (int i = j*1000; i < (j+1)*1000; i++)
    {
      node_d = database["d" + to_string(i)];
      read(node_d, descriptors);
      sizes.push_back(descriptors.rows);
    }

    std::cout << "The number of images is " <<  sizes.size() << std::endl;
    database.release();
  }

  // Retrive information about cluster numbers
  Mat labels;
  FileStorage clustering_info("clusters.yml", FileStorage::READ);
  FileNode clusters = clustering_info["labels"];
  read(clusters, labels);

  // Build words and apply metrics
  vector< vector<float> > result;
  vector<float> idf_values;
  int nonempty_rows = tf(labels, sizes, result);
  tf_idf(result, nonempty_rows, idf_values);

  // Store the result
  FileStorage tf_idf_file("words.yml", FileStorage::WRITE);
  write(tf_idf_file, "number of nonempty rows", nonempty_rows);
  for (int j = 1; j < 10; j++)
  {
    for (int i = j*1000; i < (j+1)*1000; i++)
    {
      write(tf_idf_file, "word" + to_string(i), result[i - 1000]);
    }
  }
  write(tf_idf_file, "idf values", idf_values);
  tf_idf_file.release();
}
